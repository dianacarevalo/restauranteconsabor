/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utilidad;

import java.util.ArrayList;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class ColaArrayList<Generico> {

    ArrayList<Generico> colasTres;

    public ColaArrayList() {
        colasTres = new ArrayList<Generico>();
    }

    /**
     * Agrega un valor generico a la cola
     * @param valor El valor que se agregara al ArrayList
     */
    public void agregarTres(Generico valor) {
        colasTres.add(valor);
    }

    /**
     * Remueve la información de la primera pocisión de la cola
     */
    public void removerTres() {
        colasTres.remove(0);

    }

    /**
     * Obtiene la información del primer dato de la cola
     * @return la información de la primer pocisión de la cola
     */
    public Generico recuperarTres() {
        return colasTres.get(0);
    }

    /**
     * Verifica si la cola esta vacia
     * @return si la cola esta vacia retorna un true, de lo contrario 
     * retorna false
     */
    public boolean vacioTres() {
        return colasTres.isEmpty();
    }

}
