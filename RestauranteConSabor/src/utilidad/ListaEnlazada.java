package utilidad;

/**
 * Define las operaciones basicas de una lista simplemente enlazada
 *
 * @author ESTUDIANTE2601
 * @param <G> tipo génerico usado para los objetos almacenados por los nodos de
 * la lista
 */
public class ListaEnlazada<G> {

    private Nodo<G> primero;
    private Nodo<G> ultimo;

    public ListaEnlazada() {
        this.primero = null;
        this.ultimo = null;
    }

    /**
     * VERIFICA SI LA LISTA ES VACIA
     *
     * @RETURN TRUE SI ESTÁ VACIA, EN CASO CONTRARIO FALSE
     *
     */
    public boolean esVacia() {
        return (this.primero == null);
    }

    public void agregar(G dato) {
        if (esVacia()) {
            Nodo nuevo = new Nodo(dato);
            primero = nuevo;
            ultimo = nuevo;
        } else {
            Nodo nuevo = new Nodo(dato);
            ultimo.siguiente = nuevo;
            ultimo = nuevo;
        }

    }

    /*
    Cuenta los nodos de la lista
     */
    public int dimension() {
        int contador = 0;
        Nodo actual = primero;
        while (actual != null) {
            contador++; 
            actual = actual.siguiente;
        }
        return contador;
    }

    public G obtener(int indice) {
        int cont = 1;
        if (this.esVacia() || indice < 0 || indice > this.dimension() - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (indice == 0) {
            return primero.dato;
        } else {
            Nodo actual = primero.siguiente;
            while (cont < indice) {
                actual = actual.siguiente;
                cont++;
            }
            G encontrado = (G) actual.dato;
            return encontrado;
        }
    }

    /**
     * Agrega un nodo en una posición específica dentro de la lista
     *
     * @param numNodo número de la ubicación en la cual quedará el nodo
     * @param dato objeto que contiene el nodo que se agrega
     */
    public void insertar(int numNodo, G dato) {
        int cont = 1;
        Nodo nuevo = new Nodo(dato);
        if (this.esVacia() || numNodo < 0 || numNodo > this.dimension()) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (numNodo == 0) {
            Nodo actual = primero;
            primero = nuevo;
            primero.siguiente = actual;
        } else if (numNodo == dimension()) {
            ultimo.siguiente = nuevo;
            ultimo = nuevo;
        } else {
            Nodo actual = primero;
            while (cont < numNodo) {
                actual = actual.siguiente;
                cont++;
            }
            nuevo.siguiente = actual.siguiente;
            actual.siguiente = nuevo;
        }
    }
    
    /**
     * Quita un nodo de la lista
     *
     * @param indice número de la posición en la cual se encuentra el nodo que
     * quiere eliminar
     */
    public void remover(int indice) {
        int cont = 1;
        if (this.esVacia() || indice < 0 || indice > this.dimension() - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        if (indice == 0) { // si indice es cero el nodo a remover es el primero
            Nodo aux = primero.siguiente;
            primero.siguiente = null;
            primero = aux;
        } else if (indice == dimension() - 1) { // si indice es igual a dimension - 1
            Nodo actual = primero;
            while (cont < indice) {
                actual = actual.siguiente;
                cont++;
            }
            actual.siguiente = null;
            ultimo = actual;
        } else { // si indice no es cero ni es dimension - 1, el nodo a remover es intermedio        
            Nodo actual = primero;
            while (cont < indice) { // el ciclo avanza hasta el nodo anterior al que se va a remover
                actual = actual.siguiente;
                cont++;
            }
            Nodo eliminar = actual.siguiente; // referencia de nodo a eliminar
            actual.siguiente = eliminar.siguiente; // cambio de apuntador siguiente del nodo
            // que esta antes del nodo a eliminar
            eliminar.siguiente = null; // cambio de apuntador siguiente del nodo
            // a eliminar para que quede suelto de la lista
        }
    }
}
