/**
 * Define los objetos nodo a ser contenidos por una lista simplemente enlazada
 * @param G tipo generico para el objeto que será guardado en el nodo
 */
package utilidad;


public class Nodo <G>{
    protected G dato;
    protected Nodo <G> siguiente;
    
    public Nodo(G dato){
        this.dato=dato;
        siguiente=null;
    }
    
}
