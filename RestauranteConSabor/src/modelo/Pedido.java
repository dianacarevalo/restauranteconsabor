/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Pedido {

    private String numeroPedido;
    private LinkedList<Comida> comidas;
    private LinkedList<Bebida> bebidas;

    public Pedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
        comidas = new LinkedList<>();
        bebidas = new LinkedList<>();
    }

    public String getNumeroPedido() {
        return numeroPedido;
    }

    public void setNumeroPedido(String numeroPedido) {
        this.numeroPedido = numeroPedido;
    }

    public LinkedList<Comida> getComidas() {
        return comidas;
    }

    public void setComidas(LinkedList<Comida> comidas) {
        this.comidas = comidas;
    }

    public LinkedList<Bebida> getBebidas() {
        return bebidas;
    }

    public void setBebidas(LinkedList<Bebida> bebidas) {
        this.bebidas = bebidas;
    }

    /**
     * ========================================================================
     * Agrega una comida a un pedido
     * ========================================================================
     *
     * @param c comida a agregar
     */
    public void crearComida(Comida c) {
        comidas.add(c);
    }

    /**
     * ========================================================================
     * Agrega una bebida a un pedido
     * ========================================================================
     *
     * @param b bebida a agregar
     */
    public void crearBebida(Bebida b) {
        bebidas.add(b);
    }

    /**
     * ========================================================================
     * Calcula el valor total del pedido
     * ========================================================================
     *
     * @return valor total del pedido
     */
    public double calcularValorPedido() {
        double valor = 0;
        for (Comida comida : comidas) {
            if (comida != null) {
                valor += comida.precio;
            }
        }
        for (Bebida bebida : bebidas) {
            if (bebida != null) {
                valor += bebida.precio;
            }
        }
        return valor;
    }

    /**
     * ========================================================================
     * Cuenta cuántas veces se encuentra una comida dentro del listado comidas
     * ========================================================================
     *
     * @param producto nombre de la comida a contar
     * @return número de veces encontrada
     */
    public int contarComidaPedido(String producto) {
        int contador = 0;
        for (int i = 0; i < comidas.size(); i++) {
            if (comidas.get(i).getNombre().equals(producto)) {
                contador++;
            }
        }
        return contador;
    }

    /**
     * ========================================================================
     * Cuenta cuántas veces se encuentra una bebida dentro del listado bebidas
     * ========================================================================
     *
     * @param producto nombre de la bebida a contar
     * @return número de veces encontrada
     */
    public int contarBebidaPedido(String producto) {
        int contador = 0;
        for (int i = 0; i < bebidas.size(); i++) {
            if (bebidas.get(i).getNombre().equals(producto)) {
                contador++;
            }
        }
        return contador;
    }

    /**
     * ========================================================================
     * Construye un arreglo con las comidas únicas de un pedido y la cantidad
     * ========================================================================
     *
     * @return arreglo con el listado
     */
    public ArrayList comidasUnicas() {
        int contador = 1;
        ArrayList<String> productosUnicos = new ArrayList<>();
        for (int i = 0; i < comidas.size(); i++) {
            int num = contarComidaPedido(comidas.get(i).getNombre());
            String unico = comidas.get(i).getNombre() + ", " + num;
            if (productosUnicos.isEmpty()) {
                productosUnicos.add(unico);
            } else {
                int existe = 0;
                for (String prodUnico : productosUnicos) {
                    if (prodUnico.equals(unico)) {
                        existe++;
                    }
                }
                if (existe == 0) {
                    productosUnicos.add(unico);
                }
            }
        }
        return productosUnicos;
    }

    /**
     * ========================================================================
     * Construye un arreglo con las bebidas únicas de un pedido y la cantidad
     * ========================================================================
     *
     * @return arreglo con el listado
     */
    public ArrayList bebidasUnicas() {
        int contador = 1;
        ArrayList<String> productosUnicos = new ArrayList<>();
        for (int i = 0; i < bebidas.size(); i++) {
            int num = contarBebidaPedido(bebidas.get(i).getNombre());
            String unico = bebidas.get(i).getNombre() + ", " + num;
            if (productosUnicos.isEmpty()) {
                productosUnicos.add(unico);
            } else {
                int existe = 0;
                for (String prodUnico : productosUnicos) {
                    if (prodUnico.equals(unico)) {
                        existe++;
                    }
                }
                if (existe == 0) {
                    productosUnicos.add(unico);
                }
            }
        }
        return productosUnicos;
    }
}
