/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Producto implements Comparable<Producto> {

    protected String codigo;
    protected String nombre;
    protected double precio;
    protected String estado;

    public Producto(String codigo, String nombre, double precio) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.estado = "Activo";

    }

    public String getCodigo() {
        return codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public String getEstado() {
        return estado;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int compareTo(Producto p) {
        return this.nombre.compareTo(p.getNombre());
    }

    /**
     * ========================================================================
     * Actualizar el precio de un producto
     * ========================================================================
     *
     * @param precioNuevo del producto
     */
    public void actualizarPrecioProducto(double precioNuevo) {
        this.precio = precioNuevo;
    }

    /**
     * ========================================================================
     * Cambiar el estado de un producto
     * ========================================================================
     */
    public void cambiarEstadoProducto() {
        if (this.estado.equals("Activo")) {
            this.estado = "Inactivo";
        } else {
            this.estado = "Activo";
        }
    }

}
