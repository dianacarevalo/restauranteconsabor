/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Comida extends Producto {
    
    private String nivelDePicante;
    private String tipo;

    public Comida(String nivelDePicante, String tipo, String codigo, String nombre, double precio) {
        super(codigo, nombre, precio);
        this.nivelDePicante = nivelDePicante;
        this.tipo = tipo;
    }

    public String getNivelDePicante() {
        return nivelDePicante;
    }

    public String getTipo() {
        return tipo;
    }

    public void setNivelDePicante(String nivelDePicante) {
        this.nivelDePicante = nivelDePicante;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

}
