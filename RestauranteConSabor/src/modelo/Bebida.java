    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Bebida extends Producto{
    
    private boolean contieneAlcohol;

    public Bebida(boolean contieneAlcohol, String codigo, String nombre, double precio) {
        super(codigo, nombre, precio);
        this.contieneAlcohol = contieneAlcohol;
    }

    public boolean isContieneAlcohol() {
        return contieneAlcohol;
    }

    public void setContieneAlcohol(boolean contieneAlcohol) {
        this.contieneAlcohol = contieneAlcohol;
    }
    
    
    
}
