/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import utilidad.ListaEnlazada;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Restaurante {

    private ListaEnlazada<Pedido> pedidos;
    private ListaEnlazada<Producto> productos;
    private Mesa[] mesas;

    public Restaurante() {
        pedidos = new ListaEnlazada<>();
        productos = new ListaEnlazada<>();
        mesas = new Mesa[15];
    }

    public ListaEnlazada<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ListaEnlazada<Producto> productos) {
        this.productos = productos;
    }

    public ListaEnlazada<Pedido> getPedidos() {
        return pedidos;
    }

    public void setPedidos(ListaEnlazada<Pedido> pedidos) {
        this.pedidos = pedidos;
    }

    public Mesa[] getMesas() {
        return mesas;
    }

    public void setMesas(Mesa[] mesas) {
        this.mesas = mesas;
    }

    /**
     * ========================================================================
     * Crear un nuevo producto en la lista de productos del restaurante
     * ========================================================================
     *
     * @param p producto a crear
     */
    public void crearProducto(Producto p) {
        productos.agregar(p);
    }

    /**
     * ========================================================================
     * Buscar un producto en la lista de productos a partir de su nombre
     * ========================================================================
     *
     * @param nombre del producto a buscar
     * @return el producto si lo encuentra, de lo contrario null
     */
    public Producto buscarProductoNombre(String nombre) {
        for (int i = 0; i < productos.dimension(); i++) {
            if (productos.obtener(i) != null) {
                if (productos.obtener(i).getNombre().equals(nombre)) {
                    return productos.obtener(i);
                }
            }
        }
        return null;
    }

    /**
     * ========================================================================
     * Buscar un producto en la lista de productos a partir de su código
     * ========================================================================
     *
     * @param codigo del producto a buscar
     * @return el producto si lo encuentra, de lo contrario null
     */
    public Producto buscarProductoCodigo(String codigo) {
        for (int i = 0; i < productos.dimension(); i++) {
            if (productos.obtener(i) != null) {
                if (productos.obtener(i).getCodigo().equals(codigo)) {
                    return productos.obtener(i);
                }
            }
        }
        return null;
    }

    /**
     * ========================================================================
     * Crear un nuevo pedido en el restaurante
     * ========================================================================
     *
     * @param p pedido a crear
     */
    public void registrarPedido(Pedido p) {
        pedidos.agregar(p);
    }

    /**
     * ========================================================================
     * Consulta un pedido de una mesa específica
     * ========================================================================
     *
     * @param numpedido número de pedido que se va a consultar
     * @return el detalle del pedido si lo encuentra, de lo contrario null
     */
    public Pedido consultarPedido(String numpedido) {
        for (int i = 0; i < pedidos.dimension(); i++) {
            if (pedidos.obtener(i) != null) {
                if (pedidos.obtener(i).getNumeroPedido().equals(numpedido)) {
                    return pedidos.obtener(i);
                }
            }
        }
        return null;
    }

    /**
     * ========================================================================
     * Retirar el pedido del listado de pedidos del restaurante
     * ========================================================================
     *
     * @param numpedido número de pedido del pedido que se va a retirar
     */
    public void retirarPedido(String numpedido) {
        for (int i = 0; i < pedidos.dimension(); i++) {
            if (pedidos.obtener(i) != null) {
                if (pedidos.obtener(i).getNumeroPedido().equals(numpedido)) {
                    pedidos.remover(i);
                }
            }
        }
    }

    /**
     * ========================================================================
     * Calcula el valor de todos los pedidos activos
     * ========================================================================
     *
     * @return valor total de los pedidos activos
     */
    public double valorTotalPedidos() {
        double valorTotal = 0;
        for (int i = 0; i < pedidos.dimension(); i++) {
            if (pedidos.obtener(i) != null) {
                valorTotal += pedidos.obtener(i).calcularValorPedido();
            }
        }
        return valorTotal;
    }

    /**
     * ========================================================================
     * Cuenta la cantidad de comidas registradas en el listado de productos
     * ========================================================================
     *
     * @return cantidad de comidas registradas
     */
    public int contarComidas() {
        int cantidad = 0;
        for (int i = 0; i < productos.dimension(); i++) {
            if (productos.obtener(i) != null) {
                if (productos.obtener(i) instanceof Comida) {
                    cantidad++;
                }
            }
        }
        return cantidad;

    }

    /**
     * ========================================================================
     * Cuenta la cantidad de bebidas registradas en el listado de productos
     * ========================================================================
     *
     * @return cantidad de bebidas registradas
     */
    public int contarBebidas() {
        int cantidad = 0;
        for (int i = 0; i < productos.dimension(); i++) {
            if (productos.obtener(i) != null) {
                if (productos.obtener(i) instanceof Bebida) {
                    cantidad++;
                }
            }
        }
        return cantidad;
    }

    /**
     * ========================================================================
     * Recorre toda la lista de Productos en busca de comidas con el nivel de
     * picante solicitado
     * ========================================================================
     *
     * @param nivelDePicante El nivel del picante solicitado
     * @return Un ArrayList con las comidas filtradas por el nivel de picante
     */
    public ArrayList comidasConPicante(String nivelDePicante) {
        ArrayList<Producto> comidasPicantes = new ArrayList<>();
        if (!productos.esVacia()) {
            for (int i = 0; i < productos.dimension(); i++) {
                Producto p = productos.obtener(i);
                if (p instanceof Comida) {
                    if (((Comida) p).getNivelDePicante().equals(nivelDePicante)) {
                        comidasPicantes.add(productos.obtener(i));
                    }
                }
            }
        }
        return comidasPicantes;
    }

    /**
     * ========================================================================
     * Carga un archivo con la información de los productos
     * ========================================================================
     *
     * @throws IOException Por si no se cumple con los parámetros necesarios.
     */
    public void cargarArchivoProductos() throws IOException {
        FileReader entrada = new FileReader("src/imagenes/productos.txt");
        BufferedReader archivo = new BufferedReader(entrada);
        String linea = archivo.readLine();
        while (linea != null) {
            String datos[] = linea.split(",");
            String tipo = datos[0];
            if (tipo.equals("Comida")) {
                String codigo = "PRC" + (contarComidas() + 1);
                String nombre = datos[1].toUpperCase();
                double precio = Double.parseDouble(datos[2]);
                String nivelDePicante = datos[3];
                String tipoDeComida = datos[4];
                Comida c = new Comida(nivelDePicante, tipoDeComida, codigo, nombre, precio);
                productos.agregar(c);
            } else if (tipo.equals("Bebida")) {
                String codigo = "PRB" + (contarBebidas() + 1);
                String nombre = datos[1].toUpperCase();
                double precio = Double.parseDouble(datos[2]);
                String contieneAlcohol = datos[3];
                boolean alcohol;
                if (contieneAlcohol.equals("Si")) {
                    alcohol = true;
                } else {
                    alcohol = false;
                }
                Bebida b = new Bebida(alcohol, codigo, nombre, precio);
                productos.agregar(b);
            }
            linea = archivo.readLine();
        }
        archivo.close();
    }

    /**
     * ========================================================================
     * Recorre toda la lista de Productos en busca de bebidas con el nivel de
     * alcohol requerido
     * ========================================================================
     *
     * @param alcohol indica si se buscan bebidas con o sin alcohol
     * @return Un ArrayList con las bebidas filtradas por el dato alcohol
     */
    public ArrayList bebidasAlcoholicas(String alcohol) {
        boolean a = true;
        if (alcohol.equals("Si")) {
            a = true;
        } else if (alcohol.equals("No")) {
            a = false;
        }
        ArrayList<Producto> bebidasEmbriagantes = new ArrayList<>();
        if (!productos.esVacia()) {
            for (int i = 0; i < productos.dimension(); i++) {
                Producto p = productos.obtener(i);
                if (p instanceof Bebida) {
                    if (((Bebida) p).isContieneAlcohol() == a) {
                        bebidasEmbriagantes.add(productos.obtener(i));
                    }
                }
            }
        }
        return bebidasEmbriagantes;
    }

    /**
     * ========================================================================
     * Cambia de precio del producto
     * ========================================================================
     *
     * @param nombreProducto El nombre del producto que se va a cambiar
     * @param precio nuevo precio del producto
     * @return El resultado de la operación o null si no encuentra el producto
     */
    public String cambiarPrecio(String nombreProducto, double precio) {
        Producto p = buscarProductoNombre(nombreProducto);
        if (p != null) {
            p.setPrecio(precio);
            return "El nuevo precio es: $" + precio;
        }
        return null;
    }

    /**
     * ========================================================================
     * Genera el archivo de las comidas por el tipo elegido
     * ========================================================================
     *
     * @param archivo, archivo creado y abierto para escritura
     * @param tipo, El tipo de comida que requiere el usuario
     * @throws IOException, en caso de que no se realice el proceso.
     */
    public void generarArchivoProductoComida(File archivo, String tipo) throws IOException {
        PrintWriter escritor = new PrintWriter(archivo);
        escritor.println("-------------Lista de " + tipo + "----------------");
        for (int i = 0; i < productos.dimension(); i++) {
            Producto p = productos.obtener(i);
            if (p instanceof Comida) {
                if (((Comida) p).getTipo().equals(tipo)) {
                    escritor.println("");
                    escritor.println("Código del producto: " + p.getCodigo());
                    escritor.println("Nombre del producto: " + p.getNombre());
                    escritor.println("Estado del producto: " + p.getEstado());
                    escritor.println("Precio del producto: " + p.getPrecio());
                    escritor.println("Nivel de picante del producto: " + ((Comida) p).getNivelDePicante());
                    escritor.println("");
                }
            }
        }
        escritor.close();
    }

    /**
     * ========================================================================
     * Genera el archivo de productos activos
     * ========================================================================
     *
     * @param archivo, archivo creado y abierto para escritura
     * @param estado, estado de los productos a listar
     * @throws IOException, en caso de que no se realice el proceso.
     */
    public void generarArchivoProductoActivos(File archivo, String estado) throws IOException {
        PrintWriter escritor = new PrintWriter(archivo);
        escritor.println("-------------Lista general de productos " + estado.toLowerCase() + "s" + "----------------");
        for (int i = 0; i < productos.dimension(); i++) {
            Producto p = productos.obtener(i);
            if (p instanceof Comida && p.getEstado().equals(estado)) {
                escritor.println("");
                escritor.println("Código del producto: " + p.getCodigo());
                escritor.println("Nombre del producto: " + p.getNombre());
                escritor.println("Precio del producto: " + p.getPrecio());
                escritor.println("Nivel de picante del producto: " + ((Comida) p).getNivelDePicante());
                escritor.println("");
            } else if (p instanceof Bebida && p.getEstado().equals(estado)) {
                escritor.println("");
                escritor.println("Código del producto: " + p.getCodigo());
                escritor.println("Nombre del producto: " + p.getNombre());
                escritor.println("Precio del producto: " + p.getPrecio());
                String nivelAlcohol;
                if (((Bebida) p).isContieneAlcohol()) {
                    nivelAlcohol = "Si";
                } else {
                    nivelAlcohol = "No";
                }
                escritor.println("Contiene alcohol: " + nivelAlcohol);
                escritor.println("");
            }
        }
        escritor.close();
    }

    /**
     * ========================================================================
     * Genera el archivo de las bebidas existentes
     * ========================================================================
     *
     * @param archivo, archivo creado y abierto para escritura
     * @throws IOException, en caso de que no se realice el proceso.
     */
    public void generarArchivoProductoBebidas(File archivo) throws IOException {
        PrintWriter escritor = new PrintWriter(archivo);
        escritor.println("-------------Lista de bebidas----------------");
        for (int i = 0; i < productos.dimension(); i++) {
            Producto p = productos.obtener(i);
            if (p instanceof Bebida) {
                escritor.println("");
                escritor.println("Código del producto: " + p.getCodigo());
                escritor.println("Nombre del producto: " + p.getNombre());
                escritor.println("Estado del producto: " + p.getEstado());
                escritor.println("Precio del producto: " + p.getPrecio());
                String nivelAlcohol;
                if (((Bebida) p).isContieneAlcohol()) {
                    nivelAlcohol = "Si";
                } else {
                    nivelAlcohol = "No";
                }
                escritor.println("Contiene alcohol: " + nivelAlcohol);
                escritor.println("");
            }
        }
        escritor.close();
    }

    /**
     * ========================================================================
     * Recorre toda la lista de Productos en busca del producto por su tipo
     * ========================================================================
     *
     * @param tipo Es el tipo de producto que se requiere
     * @return Un arreglo con los nombres de los productos que sean del tipo
     * requerido
     */
    public String[] productosPorTipo(String tipo) {

        ArrayList<Producto> filtrados = new ArrayList<>();
        if (!productos.esVacia()) {
            for (int i = 0; i < productos.dimension(); i++) {
                Producto p = productos.obtener(i);
                if (p instanceof Comida && ((Comida) p).getTipo().equals(tipo) && p.getEstado().equals("Activo")) {
                    filtrados.add(productos.obtener(i));
                }
            }
        }
        String[] nombresProductos = new String[filtrados.size()];
        for (int i = 0; i < filtrados.size(); i++) {
            nombresProductos[i] = filtrados.get(i).getNombre();
        }
        for (int i = 0; i < filtrados.size() - 1; i++) {
            for (int j = i + 1; j < filtrados.size(); j++) {
                String productoEnI = nombresProductos[i];
                String productoEnJ = nombresProductos[j];
                if (productoEnI.compareTo(productoEnJ) > 0) {
                    nombresProductos[j] = null;
                    nombresProductos[j] = productoEnI;
                    nombresProductos[i] = null;
                    nombresProductos[i] = productoEnJ;
                }
            }
        }
        return nombresProductos;
    }

    /**
     * ========================================================================
     * Organiza en un arreglo las comidas de tipo bebida en orden alfabético
     * ========================================================================
     *
     * @return Un arreglo con los nombres de las bebidas ordenadas
     */
    public String[] bebidasOrdenadas() {

        ArrayList<Producto> filtrados = new ArrayList<>();
        if (!productos.esVacia()) {
            for (int i = 0; i < productos.dimension(); i++) {
                Producto p = productos.obtener(i);
                if (p instanceof Bebida && p.getEstado().equals("Activo")) {
                    filtrados.add(productos.obtener(i));
                }
            }
        }
        String[] nombresProductos = new String[filtrados.size()];
        for (int i = 0; i < filtrados.size(); i++) {
            nombresProductos[i] = filtrados.get(i).getNombre();
        }
        for (int i = 0; i < filtrados.size() - 1; i++) {
            for (int j = i + 1; j < filtrados.size(); j++) {
                String productoEnI = nombresProductos[i];
                String productoEnJ = nombresProductos[j];
                if (productoEnI.compareTo(productoEnJ) > 0) {
                    nombresProductos[j] = null;
                    nombresProductos[j] = productoEnI;
                    nombresProductos[i] = null;
                    nombresProductos[i] = productoEnJ;
                }
            }
        }
        return nombresProductos;
    }

/**
 * ========================================================================
 * Llena el arreglo de las mesas
 * ========================================================================
 */
    public void inicializarMesas() {
        for (int i = 0; i < mesas.length; i++) {
            mesas[i] = new Mesa("Mesa " + (i + 1));
        }
    }
/**
 * ========================================================================
 * Busca una mesa a partir de su nombre
 * ========================================================================
 * @param mesa nombre de la mesa a buscar
 * @return la mesa si la encuentra, de lo contrario null 
 */
    public Mesa buscarMesa(String mesa) {
        for (int i = 0; i < mesas.length; i++) {
            if (mesas[i].getNumeroMesa().equals(mesa)) {
                return mesas[i];
            }
        }
        return null;
    }
}
