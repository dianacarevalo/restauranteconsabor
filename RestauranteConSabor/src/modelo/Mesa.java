/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import utilidad.ListaEnlazada;

/**
 *
 * @author Diana Carolina Arevalo Rodriguez
 * @author Michael Sneyder Morales Camacho
 */
public class Mesa {

    private String numeroMesa;
    private Pedido pedido;

    private ListaEnlazada<Pedido> historicoDePedidos;

    public Mesa(String numero) {
        this.numeroMesa = numero;
        historicoDePedidos = new ListaEnlazada<>();
    }

    public ListaEnlazada<Pedido> getHistoricoDePedidos() {
        return historicoDePedidos;
    }

    public void setHistoricoDePedidos(ListaEnlazada<Pedido> historicoDePedidos) {
        this.historicoDePedidos = historicoDePedidos;
    }

    public String getNumeroMesa() {
        return numeroMesa;
    }

    public void setNumeroMesa(String numeroMesa) {
        this.numeroMesa = numeroMesa;
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    /**
     * ========================================================================
     * Crea el consecutivo del pedido de la mesa
     * ========================================================================
     *
     * @return El número de pedidos que ha tenido la mesa
     */
    public int consecutivoMesaHistorico() {
        return (historicoDePedidos.dimension() + 1);
    }

    /**
     * ========================================================================
     * Agrega un pedido al la lista histórico de pedidos de la mesa
     * ========================================================================
     */
    public void agregarHistorico(Pedido p) {
        historicoDePedidos.agregar(p);
    }

    /**
     * ========================================================================
     * Genera un archivo con la relación de pedidos que ha tenido una mesa
     * ========================================================================
     *
     * @param archivo archivo creado y abierto para escritura
     * @param inicio número de pedido desde donde se hará la consulta
     * @param fin número de pedido hasta donde se haraá la consulta
     * @param mesa nombre de la mesa que se va a consultar
     * @throws IOException
     */
    public void archivoHistoricoPedido(File archivo, int inicio, int fin, String mesa) throws IOException {
        PrintWriter escritor = new PrintWriter(archivo);
        escritor.println("-------------Lista histórico de pedidos " + mesa + "----------------");
        for (int i = 0; i < historicoDePedidos.dimension(); i++) {
            Pedido p = historicoDePedidos.obtener(i);
            int numpedido = Integer.parseInt(historicoDePedidos.obtener(i).getNumeroPedido().substring(4));
            if (numpedido >= inicio && numpedido <= fin) {
                escritor.println("");
                escritor.println("Código del pedido: " + p.getNumeroPedido());
                escritor.println("Detalle:");
                for (int j = 0; j < p.bebidasUnicas().size(); j++) {
                    escritor.println("" + p.bebidasUnicas().get(j));
                }
                for (int j = 0; j < p.comidasUnicas().size(); j++) {
                    escritor.println("" + p.comidasUnicas().get(j));
                }
                escritor.println("Valor pedido: " + p.calcularValorPedido());
                escritor.println("");
            }
        }
        escritor.close();
    }

    /**
     * ========================================================================
     * Obtener un listado con el número consecutivo de los pedidos almacenados
     * en el histórico
     * ========================================================================
     *
     * @return listado con consecutivos
     */
    public String[] numeroPedidoHistorico() {
        String[] numeros = new String[historicoDePedidos.dimension()];
        for (int i = 0; i < historicoDePedidos.dimension(); i++) {
            numeros[i] = historicoDePedidos.obtener(i).getNumeroPedido().substring(4);
        }
        return numeros;
    }

    /**
     * ========================================================================
     * Valida si la mesa tiene histórico de pedidos
     * ========================================================================
     *
     * @return true si tiene histórico, de lo contrario false
     */
    public boolean tieneHistorico() {
        if (this.historicoDePedidos.esVacia()) {
            return false;
        }
        return true;
    }
}
